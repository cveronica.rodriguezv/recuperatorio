//const { application } = require('express');
const express=require('express');
const app =express();
//middlewarses
app.use(express.json());
app.use(express.urlencoded({extended: false}));

//rutas
app.use(require('./routers/index'));

app.listen(4000);
console.log('hola servidor 4000');


// const express = require('express');

// const app = express();

// // middlewares
// app.use(express.json());
// app.use(express.urlencoded({extended: false}));

// // Routes
// app.use(require('./routers/index'));

// app.listen(4000);
// console.log('Server on port', 4000);