
const { Router } = require('express');
const router = Router();
const{ getUsers,createUser, updateUser, deleteteUser,promedioEdad, status_d }=require('../controler/index.controller');
router.get('/users',getUsers);
router.post('/users',createUser);
router.put('/users/:ci',updateUser);
router.delete('/users/:ci',deleteteUser);
router.get('/promedio',promedioEdad);
router.get('/status_d',status_d);


module.exports = router;
